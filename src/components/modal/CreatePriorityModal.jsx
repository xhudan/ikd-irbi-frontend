import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { addPriorityAction } from "../../config/redux/action/priorityAction";
import SweatAlert from "../SweetAlert";

const CreatePriorityModal = (props) => {
  const dispatch = useDispatch();
  const [formData, setFormData] = useState({
    priority_name: "",
    year: "",
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    // Year input validation can only be numbers and a maximum of 4 digits
    if (name === 'year' && (isNaN(value) || value.length > 4)) {
      return;
    }
    
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleAdd = async (e) => {
    e.preventDefault();

    const trimmedPriorityName = formData.priority_name.trim();
    const trimmedYear = formData.year.toString().trim();

    if (!trimmedPriorityName || !trimmedYear) {
      SweatAlert("Please fill all the fields", "warning");
      return;
    }

    dispatch(addPriorityAction({
      ...formData,
      priority_name: trimmedPriorityName,
      year: trimmedYear
    }));
    props.setModal(false);
  };

  const closeModal = async () => {
    props.setModal(false);
  };

  return (
    <div className="fixed m-auto z-[100] h-screen flex right-0 left-0 top-0 justify-center items-center">
      <div className="relative p-4 w-full max-w-md max-h-full">
        {/* Modal content */}
        <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
          {/* Modal header */}
          <div className="flex items-center justify-between p-4 md:p-5 border-b rounded-t dark:border-gray-600">
            <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
              Create Priority
            </h3>
            <button onClick={closeModal} className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white">
              <svg className="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6" />
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
          </div>
          {/* Modal body */}
          <form className="p-4 md:p-5">
            <div className="grid gap-4 mb-4 grid-cols-2">
              <div className="col-span-2">
                <label htmlFor="priority_name" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Priority Name</label>
                <input
                  type="text"
                  name="priority_name"
                  value={formData.priority_name}
                  placeholder="Type priority name"
                  onChange={handleInputChange}
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                />
              </div>
              <div className="col-span-2">
                <label htmlFor="year" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Year</label>
                <input
                  type="text"
                  name="year"
                  placeholder="Type year"
                  value={formData.year}
                  onChange={handleInputChange}
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                />
              </div>
            </div>
            <div className="space-x-6">
              <button
                onClick={handleAdd}
                disabled={formData.year.length < 4}
                className={`bg-${formData.year.length < 4 ? 'gray-400 cursor-not-allowed' : 'blue-700'} 
              hover:${formData.year.length < 4 ? 'bg-gray-400' : 'bg-blue-800'} 
              text-white inline-flex items-center focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center`}
              >
                Add
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default CreatePriorityModal;
