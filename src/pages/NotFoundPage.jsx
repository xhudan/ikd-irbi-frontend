import { Link } from "react-router-dom"

const NotFoundPage = () => {
  return (
    <>
      <div className="flex justify-center items-center h-screen bg-[#EF8201]">
        <div className="flex flex-col justify-center items-center bg-white shadow overflow-hidden sm:rounded-lg p-10">
          <h1 className="text-9xl font-bold text-red-600">404</h1>
          <h1 className="text-6xl font-medium py-8">Page not found</h1>
          <p className="text-2xl px-12 font-medium">The page you are looking for does not exist.</p>
          <div className="mt-10">
            <Link
              to="/"
              className="bg-[#00469B] hover:bg-red-600 text-white font-semibold px-6 py-3 rounded-md mr-6"
            >
              Home
            </Link>
          </div>
        </div>
      </div>
    </>
  );
}

export default NotFoundPage;
