import API from "../../api/baseApi";
import SweatAlert from "../../../components/SweetAlert";
import {
  ADD_INDICATOR,
  ADD_INDICATOR_ERROR,
  ADD_INDICATOR_REQUEST,
  GET_INDICATORS,
  UPDATE_INDICATOR,
  UPDATE_INDICATOR_ERROR,
  UPDATE_INDICATOR_REQUEST,
  DELETE_INDICATOR,
  DELETE_INDICATOR_ERROR,
  DELETE_INDICATOR_REQUEST
} from "./actionTypes";

export const addIndicatorAction = (priorityId, data) => {
  return async (dispatch) => {
    let resp;
    dispatch({ type: ADD_INDICATOR_REQUEST });

    await API.post(`/*/${priorityId}`, data)
      .then((response) => {
        dispatch({ type: ADD_INDICATOR, payload: response.data });
        resp = response.data.msg;
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        dispatch({ type: ADD_INDICATOR_ERROR });
        SweatAlert(message, "warning");
      })
      .finally(() => {
        if (resp) {
          SweatAlert(resp, "success", () => {
            window.location.reload();
          });
        }
      });
  };
};

export const deleteIndicatorAction = (priorityId, id) => {
  return async (dispatch) => {
    let resp;
    dispatch({ type: DELETE_INDICATOR_REQUEST });

    await API.delete(`/*/${priorityId}/${id}`)
      .then((response) => {
        dispatch({ type: DELETE_INDICATOR, payload: response.data });
        resp = response.data.msg;
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        dispatch({ type: DELETE_INDICATOR_ERROR });
        SweatAlert(message, "warning");
      })
      .finally(() => {
        if (resp) {
          SweatAlert(resp, "success", () => {
            window.location.reload();
          });
        }
      });
  };
};

export const updateIndicatorAction = (priorityId, id, data) => {
  return async (dispatch) => {
    let resp;
    dispatch({ type: UPDATE_INDICATOR_REQUEST });

    await API.put(`/*/${priorityId}/${id}`, data)
      .then((response) => {
        dispatch({ type: UPDATE_INDICATOR, payload: response.data });
        resp = response.data.msg;
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        dispatch({ type: UPDATE_INDICATOR_ERROR });
        SweatAlert(message, "warning", () => {
          window.location.reload();
        });
      })
      .finally(() => {
        if (resp) {
          SweatAlert(resp, "success", () => {
            window.location.reload();
          });
        }
      });
  };
};

export const getIndicatorsAction = (priorityId) => {
  return async (dispatch) => {
    await API.get(`/*/${priorityId}`)
      .then((response) => {
        dispatch({ type: GET_INDICATORS, payload: response.data });
      })
      .catch((error) => {
        // console.clear();
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        SweatAlert(message, "warning");
      });
  };
};
