import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { getPrioritiesYearAction } from "../config/redux/action/priorityAction";
import Navbar from "../components/Navbar";
import PriorityModal from "../components/modal/PriorityModal";
import EditVector from "../assets/edit-svgrepo-com.svg";

const Priorities = () => {
    const token = localStorage.getItem("token");
    const decodedToken = JSON.parse(atob(token.split(".")[1]));
    const userRole = decodedToken.role;

    const { y } = useParams();

    const dispatch = useDispatch();
    const [modal, setModal] = useState(false);
    const [prioritiesData, setPrioritiesData] = useState([]);
    const [modalData, setModalData] = useState();

    const { priority } = useSelector((state) => state.priorityReducer);

    useEffect(() => {
        // Check if y is a valid year (e.g., a number or any validation logic)
        const isValidYear = /^\d{4}$/.test(y);
        if (!isValidYear) {
            window.location.replace("/");
            return;
        }
        dispatch(getPrioritiesYearAction(y));
    }, [dispatch, y]);

    useEffect(() => {
        if (priority) {
            setPrioritiesData(priority);
        }
    }, [priority]);

    // Sorts prioritiesData by priority_name
    const sortedPriorities = [...prioritiesData].sort((a, b) =>
        (a.priority_name || '').localeCompare(b.priority_name || '')
    );

    // Function to handle modal opening with appropriate data
    const handleOpenModal = (id) => {
        const selected = priority.find(item => item.id === id);
        setModalData(selected);
        setModal(true);
    };

    return (
        <div className="min-h-screen bg-[#EF8201]">
            <Navbar />
            <div className="w-[80%] mx-auto py-2">
                {sortedPriorities.map((item) => (
                    <div key={item.id} className="flex items-center gap-2 text-center max-w-md mx-auto">
                        <Link to={`${item.id}`} className="p-3">
                            <div className="text-white w-72 py-4 px-6 rounded-lg bg-[#00469B] hover:bg-[#003366]">
                                {item.priority_name}
                            </div>
                        </Link>
                        {userRole === "Admin" && (
                            <button onClick={() => handleOpenModal(item.id)}>
                                <img
                                    className="invert"
                                    src={EditVector}
                                    alt="Edit"
                                    width={30}
                                    onContextMenu={(e) => e.preventDefault()}
                                />
                            </button>
                        )}
                    </div>
                ))}
            </div>
            {modal ? <PriorityModal
                id={modalData.id}
                priority_name={modalData.priority_name}
                year={modalData.year}
                setModal={setModal}
            /> : null}
        </div>
    );
}

export default Priorities;
