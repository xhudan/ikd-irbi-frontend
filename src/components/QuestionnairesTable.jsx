import React, { useState, useEffect } from "react";
import { useParams } from "react-router-dom";
import { useDispatch } from "react-redux";
import { updateQuestionnaireAction } from "../config/redux/action/questionnaireAction";
import EditVector from "../assets/edit-svgrepo-com.svg";
import SweatAlert from "./SweetAlert";

const QuestionnairesTable = (props) => {
    const { data } = props;
    const dispatch = useDispatch();
    const { indicatorId } = useParams();
    const currentUrl = window.location.pathname;

    const token = localStorage.getItem("token");
    const decodedToken = JSON.parse(atob(token.split(".")[1]));
    const userRole = decodedToken.role;

    const sortedQuestionnaireNumb = [...data].sort((a, b) =>
        a.number_of_questionnaire - b.number_of_questionnaire);

    const [formData, setFormData] = useState({});

    useEffect(() => {
        // Populate formData with initial values from data props
        const initialFormData = {};
        data.forEach((item) => {
            initialFormData[item.id] = {
                verification: item.verification || "",
                answer: item.answer || "",
                upload_verification: item.upload_verification || "",
            };
        });
        setFormData(initialFormData);
    }, [data]);

    const handleInputChange = (e, item) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [item.id]: {
                ...formData[item.id],
                [name]: value,
            },
        });
    };

    const handleFileInputChange = (e, item) => {
        const file = e.target.files[0];
        setFormData({
            ...formData,
            [item.id]: {
                ...formData[item.id],
                upload_verification: file,
            },
        });
    };

    const handleUpdate = async (item) => {
        if (!formData[item.id]?.answer) {
            SweatAlert("Please fill the answer!", "warning");
            return;
        }

        let trimmedVerification = formData[item.id]?.verification?.trim();

        // Check if the answer is "Tidak," if yes, set verification and upload_verification to empty strings
        if (formData[item.id]?.answer === "Tidak") {
            trimmedVerification = "";
            formData[item.id].upload_verification = "";
        }

        // Create a FormData object to handle file upload
        const formDataObject = new FormData();
        formDataObject.append('answer', formData[item.id]?.answer);
        formDataObject.append('verification', trimmedVerification);
        formDataObject.append('upload_verification', formData[item.id]?.upload_verification);

        dispatch(updateQuestionnaireAction(indicatorId, item.id, formDataObject));
    };

    return (
        <div className="overflow-x-auto">
            <table className="table-auto min-w-full mx-auto bg-white">
                <thead>
                    <tr>
                        <th className="border px-4 py-2">No.</th>
                        <th className="border px-4 py-2">Question</th>
                        <th className="border px-4 py-2">Answer</th>
                        <th className="border px-4 py-2">Verification</th>
                        <th className="border px-4 py-2">Upload Verification</th>
                    </tr>
                </thead>
                <tbody>
                    {sortedQuestionnaireNumb.map((item, index) => (
                        <tr key={index}>
                            <td className="text-center border px-4 py-2">{item.number_of_questionnaire}</td>
                            <td className="border px-4 py-2">{item.fill_in_questionnaire}</td>
                            <td className="text-center border px-4 py-2">
                                <select
                                    name="answer"
                                    className="bg-[#e9e8ed] px-3 py-2 rounded-md"
                                    disabled={currentUrl.includes("/export")}
                                    value={formData[item.id]?.answer || ""}
                                    onChange={(e) => handleInputChange(e, item)}
                                >
                                    <option>{formData[item.id]?.answer}</option>
                                    <option value="Ya">Ya</option>
                                    <option value="Tidak">Tidak</option>
                                </select>
                            </td>
                            <td className="border px-4 py-2">
                                <textarea name="verification" placeholder="Type verification"
                                    className="w-full break-all bg-[#e9e8ed] p-2 rounded-md resize"
                                    disabled={currentUrl.includes("/export")}
                                    value={formData[item.id]?.verification || ''}
                                    onChange={(e) => handleInputChange(e, item)} />
                            </td>
                            <td className="border px-4 py-2">
                                    <input type="file" disabled={currentUrl.includes("/export")}
                                        accept=".jpg, .png, .jpeg, .pdf"
                                        onChange={(e) => handleFileInputChange(e, item)} />
                                {item.upload_verification && (
                                    <div className="font-bold mt-2">File Uploaded:
                                    <a href={item.upload_verification} target="_blank" rel="noopener noreferrer">
                                        <span title={item.upload_verification}>
                                            {item.upload_verification.length > 15 && " ..."}
                                            {item.upload_verification.substring(item.upload_verification.length - 15)}
                                        </span>
                                    </a>
                                    </div>
                                )}
                            </td>
                            {!currentUrl.includes("/export") && (
                                <td className="border px-4 py-2">
                                    <div className="justify-around flex flex-wrap">
                                        <button onClick={() => handleUpdate(item)}
                                            disabled={index > 0 && sortedQuestionnaireNumb[index - 1]?.answer !== "Ya"}
                                            className={`text-white bg-${(index > 0 && sortedQuestionnaireNumb[index - 1]?.answer !== "Ya") ? 'gray-400 cursor-not-allowed' : 'gray-500 hover:bg-gray-600'} min-w-20 py-2 rounded-md focus:outline-none focus:shadow-outline-gray focus:border-gray-700`}
                                        >Save</button>
                                        {userRole === "Admin" && (
                                            <button onClick={() => props.modal(item.id)}>
                                                <img
                                                    className="py-2"
                                                    src={EditVector}
                                                    alt="Edit"
                                                    width={30}
                                                    height={30}
                                                    onContextMenu={(e) => e.preventDefault()}
                                                />
                                            </button>
                                        )}
                                    </div>
                                </td>
                            )}
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
};

export default QuestionnairesTable;
