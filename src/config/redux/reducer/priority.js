import {
  ADD_PRIORITY,
  ADD_PRIORITY_ERROR,
  ADD_PRIORITY_REQUEST,
  BULK_CREATE_DATA,
  BULK_CREATE_DATA_ERROR,
  BULK_CREATE_DATA_REQUEST,
  GET_PRIORITIES,
  GET_PRIORITIES_YEAR,
  GET_ALL_DATA_BY_YEAR,
  UPDATE_PRIORITY,
  UPDATE_PRIORITY_ERROR,
  UPDATE_PRIORITY_REQUEST,
  DELETE_PRIORITY,
  DELETE_PRIORITY_ERROR,
  DELETE_PRIORITY_REQUEST
} from "../action/actionTypes";

const initialState = {
  priority: [],
  isLoading: false,
  isSuccess: false,
};

const priority = (state = initialState, action) => {
  switch (action.type) {
    case ADD_PRIORITY:
      return {
        ...state,
        isLoading: false,
        priority: action.payload.data,
        isSuccess: true,
      };
    case ADD_PRIORITY_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case ADD_PRIORITY_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case BULK_CREATE_DATA:
      return {
        ...state,
        isLoading: false,
        priority: action.payload.data,
        isSuccess: true,
      };
    case BULK_CREATE_DATA_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case BULK_CREATE_DATA_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case GET_PRIORITIES:
      return {
        ...state,
        isLoading: false,
        priority: action.payload.data,
      };
    case GET_PRIORITIES_YEAR:
      return {
        ...state,
        isLoading: false,
        priority: action.payload.data,
      };
    case GET_ALL_DATA_BY_YEAR:
      return {
        ...state,
        isLoading: false,
        priority: action.payload.data,
      };

    case UPDATE_PRIORITY:
      return {
        ...state,
        isLoading: false,
        priority: action.payload.data,
        isSuccess: true,
      };
    case UPDATE_PRIORITY_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case UPDATE_PRIORITY_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case DELETE_PRIORITY:
      return {
        ...state,
        isLoading: false,
        priority: action.payload.data,
        isSuccess: true,
      };
    case DELETE_PRIORITY_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case DELETE_PRIORITY_ERROR:
      return {
        ...state,
        isLoading: false,
      };
    default:
      return state;
  }
};

export default priority;
