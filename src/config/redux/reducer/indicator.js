import {
  ADD_INDICATOR,
  ADD_INDICATOR_ERROR,
  ADD_INDICATOR_REQUEST,
  GET_INDICATORS,
  UPDATE_INDICATOR,
  UPDATE_INDICATOR_ERROR,
  UPDATE_INDICATOR_REQUEST,
  DELETE_INDICATOR,
  DELETE_INDICATOR_ERROR,
  DELETE_INDICATOR_REQUEST
} from "../action/actionTypes";

const initialState = {
  indicator: [],
  isLoading: false,
  isSuccess: false,
};

const indicator = (state = initialState, action) => {
  switch (action.type) {
    case ADD_INDICATOR:
      return {
        ...state,
        isLoading: false,
        indicator: action.payload.data,
        isSuccess: true,
      };
    case ADD_INDICATOR_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case ADD_INDICATOR_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case GET_INDICATORS:
      return {
        ...state,
        isLoading: false,
        indicator: action.payload.data,
      };
      
    case UPDATE_INDICATOR:
      return {
        ...state,
        isLoading: false,
        indicator: action.payload.data,
        isSuccess: true,
      };
    case UPDATE_INDICATOR_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case UPDATE_INDICATOR_ERROR:
      return {
        ...state,
        isLoading: false,
      };
      
      case DELETE_INDICATOR:
        return {
          ...state,
          isLoading: false,
          indicator: action.payload.data,
          isSuccess: true,
        };
      case DELETE_INDICATOR_REQUEST:
        return {
          ...state,
          isLoading: true,
        };
      case DELETE_INDICATOR_ERROR:
        return {
          ...state,
          isLoading: false,
        };
    default:
      return state;
  }
};

export default indicator;
