import { Link, useLocation } from "react-router-dom";
import { useState } from "react";
import { useDispatch } from "react-redux";
import { logoutAction } from "../config/redux/action/authAction";
import BPBDCianjur from "../assets/logobpbd.png";

const Navbar = () => {
  const dispatch = useDispatch();
  const location = useLocation();

  const segments = location.pathname.split("/").filter(Boolean);
  const [navbar, setNavbar] = useState(false);

  const token = localStorage.getItem("token");
  const decodedToken = JSON.parse(atob(token.split(".")[1]));
  const userRole = decodedToken.role;

  const handleLogout = () => {
    dispatch(logoutAction());
  };

  return (
    <nav className="w-full bg-[#00469B] shadow">
      <div className="justify-between px-4 mx-auto md:items-center md:flex md:px-8">
        <div>
          <div className="flex items-center justify-between py-2 md:block">
            <Link to="/">
              <img src={BPBDCianjur} onContextMenu={(e) => e.preventDefault()} width={40} height={40} alt="" />
            </Link>
            <div className="md:hidden">
              <button
                className="p-2 text-gray-700 rounded-md outline-none focus:border-gray-400 focus:border"
                onClick={() => setNavbar(!navbar)}
              >
                {navbar ? (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-6 h-6 text-white"
                    viewBox="0 0 20 20"
                    fill="currentColor"
                  >
                    <path
                      fillRule="evenodd"
                      d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                      clipRule="evenodd"
                    />
                  </svg>
                ) : (
                  <svg
                    xmlns="http://www.w3.org/2000/svg"
                    className="w-6 h-6 text-white"
                    fill="none"
                    viewBox="0 0 24 24"
                    stroke="currentColor"
                    strokeWidth={2}
                  >
                    <path
                      strokeLinecap="round"
                      strokeLinejoin="round"
                      d="M4 6h16M4 12h16M4 18h16"
                    />
                  </svg>
                )}
              </button>
            </div>
          </div>
        </div>
        <div>
          <div
            className={`text-white font-bold py-3 md:block ${navbar ? 'block' : 'hidden'
              }`}
          >
            <ul className="items-center justify-center space-y-3 md:flex md:space-x-6 md:space-y-0">
              <li>
                <Link to="/">
                  Home
                </Link>
              </li>
              {(segments.length === 2) && (
                <li>
                  <Link to="./..">
                    Priority
                  </Link>
                </li>
              )}
              {segments.length === 3 && (
                <>
                  <li>
                    <Link to="./../..">Priority</Link>
                  </li>
                  <li>
                    <Link to=".." relative="path">Indicator</Link>
                  </li>
                </>
              )}
              {userRole === "Admin" && (
                <li>
                  <Link to="/export">
                    Export Data
                  </Link>
                </li>
              )}
              <li>
                <button onClick={handleLogout}>
                  Logout
                </button>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </nav>
  );
}

export default Navbar;
