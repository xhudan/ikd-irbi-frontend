import Swal from "sweetalert2";
import withReactContent from "sweetalert2-react-content";

const SweatAlert = (title, icon, callback) => {
  const MySwal = withReactContent(Swal);
  MySwal.fire({
    title,
    icon,
    confirmButtonText: "Ok",
  }).then((result) => {
    if (result.isConfirmed) {
      // Call the callback function if provided
      if (typeof callback === "function") {
        callback();
      }
    }
  });
};

export default SweatAlert;
