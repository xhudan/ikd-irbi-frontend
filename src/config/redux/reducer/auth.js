import {
  LOGIN,
  REGISTER,
  GET_USER,
  UPDATE_USER,
  LOGIN_REQUEST,
  REGISTER_REQUEST,
  UPDATE_USER_REQUEST,
  LOGOUT,
  LOGOUT_REQUEST,
  LOGIN_ERROR,
  REGISTER_ERROR,
  UPDATE_USER_ERROR,
} from "../action/actionTypes";

const initialState = {
  user: [],
  isLoggedIn: !!localStorage.getItem("token"),
  isLoading: false,
};

const auth = (state = initialState, action) => {
  switch (action.type) {
    case LOGIN:
      return {
        ...state,
        isLoggedIn: true,
        user: action.payload.user,
        isLoading: true,
      };
    case LOGIN_REQUEST:
      return {
        ...state,
        isLoggedIn: true,
        isLoading: true,
      };
    case LOGIN_ERROR:
      return {
        ...state,
        isLoggedIn: true,
        isLoading: false,
      };

    case REGISTER:
      return {
        ...state,
        isLoggedIn: false,
        user: action.payload.user,
        isLoading: false,
      };
    case REGISTER_REQUEST:
      return {
        ...state,
        isLoggedIn: false,
        isLoading: true,
      };
    case REGISTER_ERROR:
      return {
        ...state,
        isLoggedIn: false,
        isLoading: false,
      };

    case GET_USER:
      return {
        ...state,
        isLoggedIn: true,
        user: action.payload.user,
        isLoading: false,
      };

    case UPDATE_USER:
      return {
        ...state,
        isLoggedIn: true,
        isLoading: false,
      };
    case UPDATE_USER_REQUEST:
      return {
        ...state,
        isLoggedIn: true,
        isLoading: true,
      };
    case UPDATE_USER_ERROR:
      return {
        ...state,
        isLoggedIn: true,
        isLoading: false,
      };

    case LOGOUT:
      return {
        ...state,
        isLoggedIn: true,
        isLoading: false,
      };
    case LOGOUT_REQUEST:
      return {
        ...state,
        isLoggedIn: true,
        isLoading: true,
      };
    default:
      return state;
  }
};

export default auth;
