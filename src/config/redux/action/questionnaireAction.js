import API from "../../api/baseApi";
import SweatAlert from "../../../components/SweetAlert";
import {
  ADD_QUESTIONNAIRE,
  ADD_QUESTIONNAIRE_ERROR,
  ADD_QUESTIONNAIRE_REQUEST,
  GET_QUESTIONNAIRES,
  UPDATE_QUESTIONNAIRE,
  UPDATE_QUESTIONNAIRE_ERROR,
  UPDATE_QUESTIONNAIRE_REQUEST,
  DELETE_QUESTIONNAIRE,
  DELETE_QUESTIONNAIRE_ERROR,
  DELETE_QUESTIONNAIRE_REQUEST
} from "./actionTypes";

export const addQuestionnaireAction = (indicatorId, data) => {
  return async (dispatch) => {
    let resp;
    dispatch({ type: ADD_QUESTIONNAIRE_REQUEST });

    await API.post(`/*/*/${indicatorId}`, data)
      .then((response) => {
        dispatch({ type: ADD_QUESTIONNAIRE, payload: response.data });
        resp = response.data.msg;
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        dispatch({ type: ADD_QUESTIONNAIRE_ERROR });
        SweatAlert(message, "warning");
      })
      .finally(() => {
        if (resp) {
          SweatAlert(resp, "success", () => {
            window.location.reload();
          });
        }
      });
  };
};

export const deleteQuestionnaireAction = (indicatorId, id) => {
  return async (dispatch) => {
    let resp;
    dispatch({ type: DELETE_QUESTIONNAIRE_REQUEST });

    await API.delete(`/*/*/${indicatorId}/${id}`)
      .then((response) => {
        dispatch({ type: DELETE_QUESTIONNAIRE, payload: response.data });
        resp = response.data.msg;
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        dispatch({ type: DELETE_QUESTIONNAIRE_ERROR });
        SweatAlert(message, "warning");
      })
      .finally(() => {
        if (resp) {
          SweatAlert(resp, "success", () => {
            window.location.reload();
          });
        }
      });
  };
};

export const updateQuestionnaireAction = (indicatorId, id, data) => {
  return async (dispatch) => {
    let resp;
    dispatch({ type: UPDATE_QUESTIONNAIRE_REQUEST });

    await API.put(`/*/*/${indicatorId}/${id}`, data)
      .then((response) => {
        dispatch({ type: UPDATE_QUESTIONNAIRE, payload: response.data });
        resp = response.data.msg;
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        dispatch({ type: UPDATE_QUESTIONNAIRE_ERROR });
        SweatAlert(message, "warning");
      })
      .finally(() => {
        if (resp) {
          SweatAlert(resp, "success", () => {
            window.location.reload();
          });
        }
      });
  };
};

export const getQuestionnairesAction = (priorityId, indicatorId) => {
  return async (dispatch) => {
    await API.get(`/*/${priorityId}/${indicatorId}`)
      .then((response) => {
        dispatch({ type: GET_QUESTIONNAIRES, payload: response.data });
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        SweatAlert(message, "warning");
      });
  };
};
