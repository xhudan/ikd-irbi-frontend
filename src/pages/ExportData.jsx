import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Navbar from "../components/Navbar";
import { getPrioritiesAction, getAllDataByYearAction } from "../config/redux/action/priorityAction";
import XLSX from "sheetjs-style";
import { saveAs } from "file-saver";

const ExportData = () => {
  const dispatch = useDispatch();
  const { priority } = useSelector((state) => state.priorityReducer);

  const [selectedYear, setSelectedYear] = useState("");
  const [dataLoaded, setDataLoaded] = useState(false);

  useEffect(() => {
    dispatch(getPrioritiesAction());
  }, [dispatch]);

  const handleYearChange = (e) => {
    const selectedYearValue = e.target.value;

    // Check if the selected year is empty string and reload priority
    if (selectedYearValue === "") {
      dispatch(getPrioritiesAction());
    } else {
      setSelectedYear(selectedYearValue);
      setDataLoaded(false);
      dispatch(getAllDataByYearAction(selectedYearValue)).then(() => {
        setDataLoaded(true);
      });
    }
  };

  const priorityYear = [...new Set(priority.map((item) => item.year))];
  priorityYear.sort((a, b) => b - a);

  const exportToExcel = () => {
    // Check if data is loaded before exporting
    if (!dataLoaded) {
      return;
    }

    const workbook = XLSX.utils.book_new();

    // Add headers for the entire sheet
    const headerRow = ['PRIORITY', 'INDICATOR', 'NO. QUESTION', 'QUESTION', 'ANSWER', 'VERIFICATION', 'UPLOAD VERIFICATION'];
    const prioritySheetData = [headerRow];

    let prevPriority = null;
    let prevIndicator = null

    priority.forEach((priorityItem, priorityIndex) => {
      priorityItem.indicators.forEach((indicator, indicatorIndex) => {
        const sortedQuestionnaires = indicator.questionnaires.sort(
          (a, b) => a.number_of_questionnaire - b.number_of_questionnaire
        );

        sortedQuestionnaires.forEach((questionnaire) => {
          const priorityValue = `${priorityIndex + 1}. ${priorityItem.description}`;
          const indicatorValue = `${indicatorIndex + 1}. ${indicator.description}`;
          const rowData = [
            prevPriority === priorityValue ? null : priorityValue,
            prevIndicator === indicatorValue ? null : indicatorValue,
            questionnaire.number_of_questionnaire,
            questionnaire.fill_in_questionnaire,
            questionnaire.answer,
            questionnaire.verification,
            questionnaire.upload_verification,
          ];

          // If the current priority is the same as the previous one, merge cells
          if (prevPriority === priorityValue) {
            delete rowData[0]; // Set priority cell value to null
          }
          if (prevIndicator === indicatorValue) {
            delete rowData[0];
          }

          prioritySheetData.push(rowData);
          prevPriority = priorityValue;
          prevIndicator = indicatorValue;
        });
      });
    });

    const ThinBorder = {
      top: { style: "thin" },
      bottom: { style: "thin" },
      left: { style: "thin" },
      right: { style: "thin" }
    };

    const headerCellStyle = {
      font: {
        bold: true,
      },
      fill: {
        fgColor: { rgb: "EF8201" }
      },
      alignment: {
        horizontal: "center",
      },
      border: ThinBorder,
    };

    const worksheet = XLSX.utils.aoa_to_sheet(prioritySheetData);

    // Apply styles to each cell in the header row
    for (let col = 0; col < headerRow.length; col++) {
      const cellAddress = XLSX.utils.encode_cell({ r: 0, c: col });
      worksheet[cellAddress].s = headerCellStyle;
    }

    // Apply styles to each cell in the questionnaire section
    for (let row = 1; row < prioritySheetData.length; row++) {
      for (let col = 0; col < prioritySheetData[row].length; col++) {
        const cellAddress = XLSX.utils.encode_cell({ r: row, c: col });
        worksheet[cellAddress] = worksheet[cellAddress] || {};
        worksheet[cellAddress].s = {
          border: ThinBorder,
        };
      }
    }

    // Add the sheet to the workbook
    XLSX.utils.book_append_sheet(workbook, worksheet, `Questionnaires ${selectedYear}`);

    // Convert the workbook to blob form and save
    const excelBuffer = XLSX.write(workbook, {
      bookType: 'xlsx',
      type: 'array',
    });

    const blob = new Blob([excelBuffer], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    });

    // Save file
    saveAs(blob, `IKD IRBI KAB.CIANJUR_${selectedYear}.xlsx`);
  };

  return (
    <div className="min-h-screen bg-[#EF8201]">
      <Navbar />
      <div className="my-6 w-[90%] mx-auto">
        <div>
          <div className="flex items-center justify-center space-x-4 mb-4">
            <label className="text-white">Year:</label>
            <select
              className="p-2 border border-gray-300"
              onChange={(e) => {
                handleYearChange(e);
              }}
            >
              <option value="">Select Year</option>
              {priorityYear.map((year) => (
                <option key={year} value={year}>
                  {year}
                </option>
              ))}
            </select>
          </div>

          <div className="flex items-center justify-center mb-6">
            <button
              onClick={exportToExcel}
              className={`mx-auto text-white min-w-32 py-3 rounded-lg ${!selectedYear || !dataLoaded ? 'bg-gray-400 cursor-not-allowed' : 'bg-[#00469B] hover:bg-[#003366] focus:ring-4 focus:outline-none focus:ring-[#0066cc]'}`}
              disabled={!selectedYear || !dataLoaded}
            > Export to Excel
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ExportData;
