import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux"
import { Link } from "react-router-dom"
import { getPrioritiesAction } from "../config/redux/action/priorityAction";
import Navbar from '../components/Navbar'
// import CreatePriorityModal from "../components/modal/CreatePriorityModal";
import BulkCreateModal from "../components/modal/BulkCreateModal"
import Cover from '../assets/Rectangle-364.png'

const Home = () => {
  const token = localStorage.getItem("token");
  const decodedToken = JSON.parse(atob(token.split(".")[1]));
  const userRole = decodedToken.role;

  const dispatch = useDispatch();
  const [modal, setModal] = useState(false);
  const [prioritiesData, setPrioritiesData] = useState([]);

  const { priority } = useSelector((state) => state.priorityReducer);

  useEffect(() => {
    dispatch(getPrioritiesAction());
  }, [dispatch]);

  useEffect(() => {
    if (Array.isArray(priority)) {
      setPrioritiesData(priority);
    }
  }, [priority]);

  const priorityYear = [...new Set(prioritiesData.map((item) => item.year))];

  priorityYear.sort((a, b) => a - b);

  const handleOpenModal = () => {
    setModal(true);
  };

  return (
    <div className="min-h-screen bg-[#EF8201]">
      <Navbar />
      {modal ? <BulkCreateModal setModal={setModal} /> : null}
      <div className="w-[90%] md:w-[60%] mx-auto">
        <img src={Cover} className="my-5 w-full" alt="" />
        {userRole === "Admin" &&
          <button onClick={() => handleOpenModal()} className="items-center flex justify-center m-5 mb-5 mx-auto text-center text-white min-w-40 py-4 px-6 rounded-lg bg-gray-700 hover:bg-gray-800"
          > Add Questionnaires
          </button>
        }
        <div className="flex flex-wrap">
          {priorityYear.map((year) => (
            <Link
              key={year}
              to={`/${year}`}
              className="m-5 mx-auto text-center text-white min-w-40 py-4 px-6 rounded-lg bg-[#00469B] hover:bg-[#003366]"
            >
              <div>
                {year}
              </div>
            </Link>
          ))}
        </div>
      </div>
    </div>
  );
}

export default Home;
