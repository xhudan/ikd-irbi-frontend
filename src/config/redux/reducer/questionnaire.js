import {
  ADD_QUESTIONNAIRE,
  ADD_QUESTIONNAIRE_ERROR,
  ADD_QUESTIONNAIRE_REQUEST,
  GET_QUESTIONNAIRES,
  UPDATE_QUESTIONNAIRE,
  UPDATE_QUESTIONNAIRE_ERROR,
  UPDATE_QUESTIONNAIRE_REQUEST,
  DELETE_QUESTIONNAIRE,
  DELETE_QUESTIONNAIRE_ERROR,
  DELETE_QUESTIONNAIRE_REQUEST
} from "../action/actionTypes";

const initialState = {
  questionnaire: [],
  isLoading: false,
  isSuccess: false,
};

const questionnaire = (state = initialState, action) => {
  switch (action.type) {
    case ADD_QUESTIONNAIRE:
      return {
        ...state,
        isLoading: false,
        questionnaire: action.payload.data,
        isSuccess: true,
      };
    case ADD_QUESTIONNAIRE_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case ADD_QUESTIONNAIRE_ERROR:
      return {
        ...state,
        isLoading: false,
      };

    case GET_QUESTIONNAIRES:
      return {
        ...state,
        isLoading: false,
        questionnaire: action.payload.data,
      };
      
    case UPDATE_QUESTIONNAIRE:
      return {
        ...state,
        isLoading: false,
        questionnaire: action.payload.data,
        isSuccess: true,
      };
    case UPDATE_QUESTIONNAIRE_REQUEST:
      return {
        ...state,
        isLoading: true,
      };
    case UPDATE_QUESTIONNAIRE_ERROR:
      return {
        ...state,
        isLoading: false,
      };
      
      case DELETE_QUESTIONNAIRE:
        return {
          ...state,
          isLoading: false,
          questionnaire: action.payload.data,
          isSuccess: true,
        };
      case DELETE_QUESTIONNAIRE_REQUEST:
        return {
          ...state,
          isLoading: true,
        };
      case DELETE_QUESTIONNAIRE_ERROR:
        return {
          ...state,
          isLoading: false,
        };
    default:
      return state;
  }
};

export default questionnaire;
