import { useEffect } from "react"
import { Provider } from "react-redux"
import "./App.css"
import AppRouter from "./config/routes"
import store from "./config/redux/store"

function App() {
  useEffect(() => {
    const token = localStorage.getItem("token");

    if (token) {
      // Parse the token to extract the expiration time
      const decodedToken = JSON.parse(atob(token.split(".")[1]));
      const expirationTime = decodedToken.exp;

      if (Date.now() >= expirationTime * 1000) {
        localStorage.clear();
        window.location.replace("/");
      } else {
        // If the token is still valid, set a timeout to clear localStorage after 1 hour
        const timeout = setTimeout(() => {
          localStorage.clear();
          window.location.replace("/");
        }, 60 * 60 * 1000);

        // Return a cleanup function to clear the timeout when the component unmounts
        return () => clearTimeout(timeout);
      }
    }
  }, []);
  return (
    <Provider store={store}>
      <AppRouter />
    </Provider>
  );
}

export default App;
