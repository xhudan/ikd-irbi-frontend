import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";
import { getQuestionnairesAction } from "../config/redux/action/questionnaireAction";
import Navbar from "../components/Navbar";
import QuestionnairesTable from "../components/QuestionnairesTable";
// import CreateQuestionnaireModal from "../components/modal/CreateQuestionnaireModal";
import QuestionnaireModal from "../components/modal/QuestionnaireModal";

const Questionnaires = () => {
    // const token = localStorage.getItem("token");
    // const decodedToken = JSON.parse(atob(token.split(".")[1]));
    // const userRole = decodedToken.role;

    const dispatch = useDispatch();
    const params = useParams();

    const [modal, setModal] = useState(false);
    // const [modalAdd, setAddModal] = useState(false);
    const [questionnairesData, setQuestionnairesData] = useState([]);
    const [indicatorData, setIndicatorData] = useState();
    const [modalData, setModalData] = useState();

    const { questionnaire } = useSelector((state) => state.questionnaireReducer);

    useEffect(() => {
        if (!isNaN(params.priorityId && params.indicatorId)) {
            dispatch(getQuestionnairesAction(params.priorityId, params.indicatorId));
        } else {
            window.location.replace("/");
        }
    }, [dispatch, params.priorityId, params.indicatorId]);

    useEffect(() => {
        if (questionnaire) {
            setIndicatorData(questionnaire);
            setQuestionnairesData(questionnaire.questionnaires || []);
        }
    }, [questionnaire]);
    // questionnaires berasal dari response server (indicator include questionnaire)
    // useEffect(() => {
    //     const newData = questionnaire?.questionnaires || [];
    //     setQuestionnairesData(newData);
    // }, [questionnaire]);

    // const handleOpenAddModal = () => {
    //     setAddModal(true);
    // };

    const handleOpenModal = (id) => {
        const selected = questionnairesData.find(item => item.id === id);
        setModalData(selected);
        setModal(true);
    };

    return (
        <div className="min-h-screen bg-[#EF8201]">
            <Navbar />
            {/* {modalAdd ? <CreateQuestionnaireModal setAddModal={setAddModal} /> : null} */}
            {modal ? <QuestionnaireModal
                id={modalData.id}
                number_of_questionnaire={modalData.number_of_questionnaire}
                fill_in_questionnaire={modalData.fill_in_questionnaire}
                setModal={setModal}
            /> : null}
            <div className="w-[90%] mx-auto pb-6">
                {/* {userRole === "Admin" && <button onClick={() => handleOpenAddModal()} className="items-center flex justify-center m-5 mb-10 mx-auto text-center text-white min-w-40 p-6 rounded-lg bg-gray-700 hover:bg-gray-800">Add Questionnaire</button>} */}
                <h1 className="mt-6 text-3xl text-white font-bold">{indicatorData?.indicator_name}</h1>
                <p className="my-4 text-xl text-white">{indicatorData?.description}</p>
                <QuestionnairesTable data={questionnairesData} modal={handleOpenModal} />
            </div>
        </div>
    );
}

export default Questionnaires;
