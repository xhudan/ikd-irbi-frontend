import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, useParams } from "react-router-dom";
import { getIndicatorsAction } from "../config/redux/action/indicatorAction";
import Navbar from '../components/Navbar';
import IndicatorModal from "../components/modal/IndicatorModal";
// import CreateIndicatorModal from "../components/modal/CreateIndicatorModal";
import EditVector from "../assets/edit-svgrepo-com.svg";

const Indicators = () => {
    const token = localStorage.getItem("token");
    const decodedToken = JSON.parse(atob(token.split(".")[1]));
    const userRole = decodedToken.role;

    const dispatch = useDispatch();
    const { priorityId } = useParams();

    const [modal, setModal] = useState(false);
    // const [modalAdd, setAddModal] = useState(false);
    const [indicatorsData, setIndicatorsData] = useState([]);
    const [modalData, setModalData] = useState();

    const { indicator } = useSelector((state) => state.indicatorReducer);

    useEffect(() => {
        if (!isNaN(priorityId)) {
            dispatch(getIndicatorsAction(priorityId));
        } else {
            window.location.replace("/");
        }
    }, [dispatch, priorityId]);

    useEffect(() => {
        if (Array.isArray(indicator)) {
            setIndicatorsData(indicator);
        }
    }, [indicator]);

    // Sorts indicatorsData by indicator_name
    const sortedIndicators = [...indicatorsData].sort((a, b) =>
        (a.indicator_name || '').localeCompare(b.indicator_name || '')
    );

    // Function to handle modal opening with appropriate data
    const handleOpenModal = (id) => {
        const selected = indicator.find(item => item.id === id);
        setModalData(selected);
        setModal(true);
    };

    // const handleOpenAddModal = () => {
    //     setAddModal(true);
    // };

    return (
        <div className="min-h-screen bg-[#EF8201]">
            <Navbar />
            {/* {modalAdd ? <CreateIndicatorModal setAddModal={setAddModal} /> : null} */}
            <div className="w-[80%] mx-auto py-2">
                <div className="items-center text-center">
                    {/* {userRole === "Admin" && <button onClick={() => handleOpenAddModal()} className="items-center flex justify-center m-5 mb-10 mx-auto text-center text-white min-w-40 p-6 rounded-lg bg-gray-700 hover:bg-gray-800">Add Indicator</button>} */}
                    {sortedIndicators.map((item) => (
                        <div key={item.id} className="flex items-center gap-2 text-center max-w-md mx-auto">
                            <Link to={`${item.id}`} className="p-3">
                                <div className="text-white w-72 py-4 px-6 rounded-lg bg-[#00469B] hover:bg-[#003366]">
                                    {item.indicator_name}
                                </div>
                            </Link>
                            {userRole === "Admin" && (
                                <button onClick={() => handleOpenModal(item.id)}>
                                    <img
                                        className="invert"
                                        src={EditVector}
                                        alt="Edit"
                                        width={30}
                                        onContextMenu={(e) => e.preventDefault()}
                                    />
                                </button>
                            )}
                        </div>
                    ))}
                </div>
                {modal ? <IndicatorModal
                    id={modalData.id}
                    indicator_name={modalData.indicator_name}
                    description={modalData.description}
                    setModal={setModal}
                /> : null}
            </div>
        </div>
    );
}

export default Indicators;
