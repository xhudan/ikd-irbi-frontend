import { combineReducers } from "redux";
import authReducer from "../reducer/auth";
import priorityReducer from "../reducer/priority";
import indicatorReducer from "../reducer/indicator";
import questionnaireReducer from "../reducer/questionnaire";

export default combineReducers({
  authReducer,
  priorityReducer,
  indicatorReducer,
  questionnaireReducer
});
