import React from "react"
import { BrowserRouter, Routes, Route, Navigate } from "react-router-dom"

import Login from "../../pages/Login"
import Home from "../../pages/Home"
import Priorities from "../../pages/Priorities"
import Indicators from "../../pages/Indicators"
import Questionnaires from "../../pages/Questionnaires"
import ExportData from "../../pages/ExportData"
import NotFoundPage from "../../pages/NotFoundPage"

const AppRouter = () => {
  const token = localStorage.getItem("token");
  let userRole;

  if (token) {
    const decodedToken = JSON.parse(atob(token.split(".")[1]));
    userRole = decodedToken.role;
  }

  return (
    <BrowserRouter>
      <Routes>
        <Route path="/login" element={token ? <Navigate to="/" /> : <Login />} />
        <Route path="/" element={!token ? <Navigate to="/login" /> : <Home />} />
        <Route path="/:y" element={!token ? <Navigate to="/login" /> : <Priorities />} />
        <Route path="/:y/:priorityId" element={!token ? <Navigate to="/login" /> : <Indicators />} />
        <Route path="/:y/:priorityId/:indicatorId" element={!token ? <Navigate to="/login" /> : <Questionnaires />} />
        <Route
          path="/export"
          element={
            !token ? (
              <Navigate to="/login" />
            ) : userRole === "Admin" ? (
              <ExportData />
            ) : (
              <Navigate to="/" />
            )
          }
        />
        <Route path="*" element={<NotFoundPage />} />
      </Routes>
    </BrowserRouter>
  );
};

export default AppRouter;
