import React from "react";
import { useDispatch } from "react-redux";
import { useForm } from "react-hook-form";
import { loginAction } from "../config/redux/action/authAction";
import NavbarLogin from '../components/NavbarLogin';
import BPBDCianjur from "../assets/logobpbd.png";

const Login = () => {
  const dispatch = useDispatch();
  const { handleSubmit, register } = useForm();

  const onSubmit = (data) => {
    dispatch(loginAction(data));
  };

  return (
    <div className="min-h-screen bg-[#EF8201]">
      <NavbarLogin />
      <div className="flex flex-col mx-auto justify-center items-center py-6 gap-6 md:absolute top-[calc(50%+32px)] md:left-1/2 md:transform md:-translate-x-1/2 md:-translate-y-1/2 md:flex-row">
        <div className="flex">
          <img src={BPBDCianjur} className="w-72" alt="BPBD Kab. Cianjur" />
        </div>
        <div className="flex flex-col">
          <div className="w-full max-w-md space-y-4 bg-[#00469B] rounded-md p-6">
            <div>
              <h1 className="text-center text-2xl text-white font-bold">Login</h1>
            </div>
            <form onSubmit={handleSubmit(onSubmit)} className="mt-8 space-y-4">
              <div>
                <p className="text-white font-bold">Username</p>
                <input
                  {...register("username")}
                  type="text"
                  placeholder="Enter your username"
                  className="w-full px-4 py-2 mt-1 border-gray-300 rounded-md focus:border-indigo-500 focus:ring focus:ring-indigo-200"
                  required
                />
              </div>
              <div>
                <p className="text-white font-bold">Password</p>
                <input
                  {...register("password")}
                  type="password"
                  placeholder="Enter your password"
                  className="w-full px-4 py-2 mt-1 border-gray-300 rounded-md focus:border-indigo-500 focus:ring focus:ring-indigo-200"
                  required
                />
              </div>
              <div>
                <button
                  type="submit"
                  className="w-full px-4 py-2 font-bold text-white bg-gray-500 rounded-md hover:bg-gray-600 focus:outline-none focus:shadow-outline-gray focus:border-gray-700"
                >
                  Sign In
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
