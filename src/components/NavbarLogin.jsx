const NavbarLogin = () => {
  return (
    <div>
      <nav className="w-full bg-[#00469B] shadow">
        <div className="text-center items-center py-3 md:block">
          <h2 className="text-2xl text-white font-bold">INDEKS KAPASITAS DAERAH KAB. CIANJUR</h2>
        </div>
      </nav>
    </div>
  );
}

export default NavbarLogin;