import API from "../../api/baseApi"
import SweatAlert from "../../../components/SweetAlert"
import {
  LOGIN,
  REGISTER,
  GET_USER,
  UPDATE_USER,
  LOGIN_REQUEST,
  REGISTER_REQUEST,
  UPDATE_USER_REQUEST,
  LOGOUT_REQUEST,
  LOGOUT,
  LOGIN_ERROR,
  REGISTER_ERROR,
  UPDATE_USER_ERROR,
} from "./actionTypes";

export const loginAction = (data) => {
  return async (dispatch) => {
    dispatch({ type: LOGIN_REQUEST });
    await API.post(`/auth/login`, data)
      .then((response) => {
        dispatch({ type: LOGIN, payload: response.data });
        localStorage.setItem("token", response.data.token);
        window.location.href = "/";
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        dispatch({ type: LOGIN_ERROR });
        SweatAlert(message, "warning");
      });
  };
};

export const registerAction = (data) => {
  return async (dispatch) => {
    dispatch({ type: REGISTER_REQUEST });
    await API.post(`/auth/register`, data)
      .then((response) => {
        dispatch({ type: REGISTER, payload: response.data });
        localStorage.setItem("email", response.data.user.email);
        SweatAlert(response.data.msg, "success");
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        dispatch({ type: REGISTER_ERROR });
        SweatAlert(message, "warning");
      });
  };
};

export const getUser = () => {
  return async (dispatch) => {
    await API.get(`/auth/user/:id`)
      .then((response) => {
        dispatch({ type: GET_USER, payload: response.data });
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        SweatAlert(message, "warning");
      });
  };
};

export const updateUser = (data) => {
  return async (dispatch) => {
    dispatch({ type: UPDATE_USER_REQUEST });
    await API.put(`/auth/user/:id`, data)
      .then((response) => {
        dispatch({ type: UPDATE_USER, payload: response.data });
        SweatAlert(response.data.msg, "success");
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        dispatch({ type: UPDATE_USER_ERROR });
        SweatAlert(message, "warning");
      });
  };
};

export const logoutAction = () => {
  return async (dispatch) => {
    dispatch({ type: LOGOUT_REQUEST });
    localStorage.clear();
    dispatch({ type: LOGOUT });
    window.location.href = "/login";
  };
};
