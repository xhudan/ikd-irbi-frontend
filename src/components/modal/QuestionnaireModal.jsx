import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { useParams } from "react-router-dom";
import { updateQuestionnaireAction, deleteQuestionnaireAction } from "../../config/redux/action/questionnaireAction";
import SweatAlert from "../SweetAlert";

const QuestionnaireModal = (props) => {
  const dispatch = useDispatch();
  const { indicatorId } = useParams();

  const [formData, setFormData] = useState({
    number_of_questionnaire: props.number_of_questionnaire || "",
    fill_in_questionnaire: props.fill_in_questionnaire || "",
  });

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    if (name === 'number_of_questionnaire' && (isNaN(value) || value.length > 2)) {
        return;
    }
    
    setFormData({
      ...formData,
      [name]: value,
    });
  };

  const handleUpdate = async (e) => {
    e.preventDefault();

    const trimmedQuestionnaireNumber = formData.number_of_questionnaire.toString().trim();
    const trimmedQuestionnaire = formData.fill_in_questionnaire.trim();

    if (!trimmedQuestionnaireNumber || !trimmedQuestionnaire) {
      SweatAlert("Please fill all the fields", "warning");
      return;
    }

    dispatch(updateQuestionnaireAction(indicatorId, props.id, {
      ...formData,
      number_of_questionnaire: trimmedQuestionnaireNumber,
      fill_in_questionnaire: trimmedQuestionnaire
    }));
    
    props.setModal(false);
  };

  const handleDelete = async () => {
    dispatch(deleteQuestionnaireAction(indicatorId, props.id));
    props.setModal(false);
  };

  const closeModal = async () => {
    props.setModal(false);
  };

  return (
    <div className="fixed m-auto z-[100] h-screen flex right-0 left-0 top-0 justify-center items-center">
      <div className="relative p-4 w-full max-w-md max-h-full">
        {/* Modal content */}
        <div className="relative bg-white rounded-lg shadow dark:bg-gray-700">
          {/* Modal header */}
          <div className="flex items-center justify-between p-4 md:p-5 border-b rounded-t dark:border-gray-600">
            <h3 className="text-lg font-semibold text-gray-900 dark:text-white">
              Edit Questionnaire
            </h3>
            <button onClick={closeModal} className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm w-8 h-8 ms-auto inline-flex justify-center items-center dark:hover:bg-gray-600 dark:hover:text-white">
              <svg className="w-3 h-3" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                <path stroke="currentColor" strokeLinecap="round" strokeLinejoin="round" strokeWidth="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6" />
              </svg>
              <span className="sr-only">Close modal</span>
            </button>
          </div>
          {/* Modal body */}
          <form className="p-4 md:p-5">
            <div className="grid gap-4 mb-4 grid-cols-2">
              <div className="col-span-2">
                <label htmlFor="number_of_questionnaire" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Questionnaire Number</label>
                <input
                  type="text"
                  name="number_of_questionnaire"
                  value={formData.number_of_questionnaire}
                  onChange={handleInputChange}
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                />
              </div>
              <div className="col-span-2">
                <label htmlFor="fill_in_questionnaire" className="block mb-2 text-sm font-medium text-gray-900 dark:text-white">Questionnaire</label>
                <input
                  type="text"
                  name="fill_in_questionnaire"
                  value={formData.fill_in_questionnaire}
                  onChange={handleInputChange}
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-primary-600 focus:border-primary-600 block w-full p-2.5 dark:bg-gray-600 dark:border-gray-500 dark:placeholder-gray-400 dark:text-white dark:focus:ring-primary-500 dark:focus:border-primary-500"
                />
              </div>
            </div>
            <div className="space-x-6">
              <button
                onClick={handleUpdate}
                className="text-white inline-flex items-center bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
              >
                Update
              </button>
              <button
                onClick={handleDelete}
                className="text-white inline-flex items-center bg-red-700 hover:bg-red-800 focus:ring-4 focus:outline-none focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-red-600 dark:hover:bg-red-700 dark:focus:ring-red-800"
              >
                Delete
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

export default QuestionnaireModal;
