import API from "../../api/baseApi";
import SweatAlert from "../../../components/SweetAlert";
import {
  ADD_PRIORITY,
  ADD_PRIORITY_ERROR,
  ADD_PRIORITY_REQUEST,
  BULK_CREATE_DATA,
  BULK_CREATE_DATA_ERROR,
  BULK_CREATE_DATA_REQUEST,
  GET_PRIORITIES,
  GET_PRIORITIES_YEAR,
  GET_ALL_DATA_BY_YEAR,
  UPDATE_PRIORITY,
  UPDATE_PRIORITY_ERROR,
  UPDATE_PRIORITY_REQUEST,
  DELETE_PRIORITY,
  DELETE_PRIORITY_ERROR,
  DELETE_PRIORITY_REQUEST
} from "./actionTypes";

export const addPriorityAction = (data) => {
  return async (dispatch) => {
    let resp;
    dispatch({ type: ADD_PRIORITY_REQUEST });

    await API.post(`/`, data)
      .then((response) => {
        dispatch({ type: ADD_PRIORITY, payload: response.data });
        resp = response.data.msg;
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        dispatch({ type: ADD_PRIORITY_ERROR });
        SweatAlert(message, "warning");
      })
      .finally(() => {
        if (resp) {
          SweatAlert(resp, "success", () => {
            window.location.reload();
          });
        }
      });
  };
};

export const bulkCreateData = (data) => {
  return async (dispatch) => {
    let resp;
    dispatch({ type: BULK_CREATE_DATA_REQUEST });

    await API.post(`/bulkcreate`, data)
      .then((response) => {
        dispatch({ type: BULK_CREATE_DATA, payload: response.data });
        resp = response.data.msg;
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        dispatch({ type: BULK_CREATE_DATA_ERROR });
        SweatAlert(message, "warning");
      })
      .finally(() => {
        if (resp) {
          SweatAlert(resp, "success", () => {
            window.location.reload();
          });
        }
      });
  };
};

export const deletePriorityAction = (id) => {
  return async (dispatch) => {
    let resp;
    dispatch({ type: DELETE_PRIORITY_REQUEST });

    await API.delete(`/*/${id}`)
      .then((response) => {
        dispatch({ type: DELETE_PRIORITY, payload: response.data });
        resp = response.data.msg;
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        dispatch({ type: DELETE_PRIORITY_ERROR });
        SweatAlert(message, "warning");
      })
      .finally(() => {
        if (resp) {
          SweatAlert(resp, "success", () => {
            window.location.reload();
          });
        }
      });
  };
};

export const updatePriorityAction = (id, data) => {
  return async (dispatch) => {
    let resp;
    dispatch({ type: UPDATE_PRIORITY_REQUEST });

    await API.put(`/*/${id}`, data)
      .then((response) => {
        dispatch({ type: UPDATE_PRIORITY, payload: response.data });
        resp = response.data.msg;
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        dispatch({ type: UPDATE_PRIORITY_ERROR });
        SweatAlert(message, "warning");
      })
      .finally(() => {
        if (resp) {
          SweatAlert(resp, "success", () => {
            window.location.reload();
          });
        }
      });
  };
};

export const getPrioritiesAction = () => {
  return async (dispatch) => {
    await API.get(`/`)
      .then((response) => {
        dispatch({ type: GET_PRIORITIES, payload: response.data });
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        SweatAlert(message, "warning");
      });
  };
};

export const getPrioritiesYearAction = (y) => {
  return async (dispatch) => {
    await API.get(`/${y}`)
      .then((response) => {
        dispatch({ type: GET_PRIORITIES_YEAR, payload: response.data });
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        SweatAlert(message, "warning", () => {
          window.location.replace("/");
        });
      });
  };
};

export const getAllDataByYearAction = (y) => {
  return async (dispatch) => {
    await API.get(`/export?y=${y}`)
      .then((response) => {
        dispatch({ type: GET_ALL_DATA_BY_YEAR, payload: response.data });
      })
      .catch((error) => {
        const message = (error.response && error.response.data && error.response.data.msg) || error.msg || error.toString();
        SweatAlert(message, "warning");
      });
  };
};
